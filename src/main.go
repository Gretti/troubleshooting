package main

import (
	"encoding/json"
	_ "github.com/lib/pq"
	ioutil "io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sofrance.fr/gnn/src/backend"
	"sofrance.fr/gnn/src/redshift"
	"sofrance.fr/gnn/src/repository"
	"sofrance.fr/gnn/src/utils"
	"strings"
	"time"
)

// Defines the max date for activation (e.g. if analysing values for billing of february 2021, limitDate equals 2021-03-01 00:00:00.000000
const (
	username  = "5c86800625643267a9c42419"
	passwd    = "42eb21fa04c2297210292eeae42f397d"
	limitDate = "2022-02-01 00:00:00.000000"
	dPB       = "2006-01-02 15:04:05.000000"
	outputDir = "C:\\Gretti\\work\\Billy\\AutoTroubleShooting"
)

func main() {
	// names := []string{
	//	"RUNA_OPP ZH 1364_Atlas",
	//	"RUNA_OPP ZH 1365_Atlas Wifi",
	//	"BEEPINGS_PLUS EUROPE ZH1412",
	//	"beepings_306d",
	//	"FFLY4U_ZH1218_EUROPE_PLUS",
	//	"GREENCITYZEN_FRANCE_PLUS_ZH1522",
	//	"green_ci_6509_ba1e",
	//	"FLIPR_EUROPE_PLUS_ZH1561",
	//	"SOGEDO_Projet EXCIDEUIL_2020",
	//	"SOGEDO_Projet REVERMONT_2020",
	//	"SOGEDO_Projet Septeme_2020",
	//	"SOGEDO_TOUSSIEU",
	//	"MERCIYANIS ZH1731 PLUS",
	//	"Traqueur_Plus_EUR_C_ZH1823",
	//	"HIBOO_PLUS_EUROPE_ZH1856",
	//	"hiboo_3686_4cfa",
	//	"alizent_44b1_6a31",
	//	"biyotte_4b83_7720",
	//	"hdsn_sas_3237_3b7d",
	//	"pyrescom_3c33_599f",
	//	"ratdar_69cb_bf9d",
	//	"sencrop_3130_3134",
	//	"sensolus_3663_4b18",
	//	"sensolus_4d6c_7a96",
	//	"sensolus_4d6c_7a97",
	//	"softysof_34b8_444e",
	//	"suez_gro_4287_6628",
	//	"tecsol_44d3_6a55",
	//	"sigfox_u_4a92_7555",
	//	"sigfox_u_3ac8_5368",
	//}
	names := []string{
		"CACG_FRANCE_BASIC_V1_ZH2072_CORP-34608",
	}

	referenceDate, err := time.Parse(dPB, limitDate)
	utils.CheckErr(err)

	baseDir := filepath.Join(outputDir, (referenceDate.Month() - 1).String())
	if _, err := os.Stat(baseDir); os.IsNotExist(err) {
		err = os.Mkdir(baseDir, 0666)
	}
	endDate := referenceDate.Add(-1 * time.Microsecond)

	startDate := referenceDate.AddDate(0, -1, 0)

	for _, name := range names {
		log.Printf("Processing %s", name)

		// Retrieve contract
		contract, err := backend.FindContract(baseDir, name, username, passwd)
		if err != nil || len(contract.Data) == 0 {
			log.Printf("Contract %s not found! Skipping...", name)
			continue
		}

		log.Printf("Found contract %s", contract.Data[0].ID)

		// Retrieve all devices from contract on the backend
		devicesCorp, err := backend.FindDevices(contract.Data[0].ID, baseDir, name, username, passwd)
		utils.CheckErr(err)
		log.Printf("Found %d devices with valid tokens in Corp", len(devicesCorp.Data))

		// Keep devices with an activation fee in chosen month
		var activatedDevicesCorp []repository.DeviceCorp
		var renewalDevicesCorp []repository.DeviceCorp
		for _, d := range devicesCorp.Data {
			dad := time.Unix(0, d.ActivationTime*int64(time.Millisecond))
			if dad.Month() == startDate.Month() {
				if dad.Year() == startDate.Year() {
					activatedDevicesCorp = append(activatedDevicesCorp, d)
				} else {
					renewalDevicesCorp = append(renewalDevicesCorp, d)
				}
			}
		}
		log.Printf("Found %d devices activated in Corp in %d/%d", len(activatedDevicesCorp), endDate.Month(), endDate.Year())
		log.Printf("Found %d devices renewal in Corp in %d/%d", len(renewalDevicesCorp), endDate.Month(), endDate.Year())

		// Look for activated devices in Billy (Redshift)
		db, err := redshift.Connect()
		utils.CheckErr(err)

		q := redshift.BuildDeviceRequest(contract, startDate.Year(), int(startDate.Month()))

		rows, err := db.Query(q)
		utils.CheckErr(err)

		var devicesBilly []repository.DeviceBilly
		var activatedDevicesBilly []repository.DeviceBilly
		var renewalDevicesBilly []repository.DeviceBilly
		for rows.Next() {
			var d repository.DeviceBilly
			if err = rows.Scan(&d.IdHex, &d.ContractID, &d.TokenStartTime, &d.TokenEndTime, &d.StartInMonth, &d.StartInMonthWithoutYear, &d.Month, &d.Year, &d.NipIDActivation, &d.IsBillable); err != nil {
				utils.CheckErr(err)
			}
			devicesBilly = append(devicesBilly, d)
			if d.IsBillable == 1 &&
				d.TokenStartTime.Before(endDate) &&
				(d.TokenEndTime.IsZero() || d.TokenEndTime.After(startDate)) {
				if d.StartInMonth == 1 {
					activatedDevicesBilly = append(activatedDevicesBilly, d)
				} else if d.StartInMonthWithoutYear == 1 {
					renewalDevicesBilly = append(renewalDevicesBilly, d)
				}
			}
		}

		err = rows.Close()
		utils.CheckErr(err)

		err = db.Close()
		utils.CheckErr(err)
		log.Printf("Found %d devices activated in Billy in %d/%d", len(activatedDevicesBilly), endDate.Month(), endDate.Year())
		log.Printf("Found %d devices renewal in Billy in %d/%d", len(renewalDevicesBilly), endDate.Month(), endDate.Year())

		devBilly := append(renewalDevicesBilly, activatedDevicesBilly...)
		devCorp := append(renewalDevicesCorp, activatedDevicesCorp...)

		corpsNotInBilly, billyNotInCorp := repository.Diff(&devCorp, &devBilly)

		// Produce report
		report := repository.Report{
			NbDevicesCorp:           len(devicesCorp.Data),
			NbDevicesBilly:          len(devicesBilly),
			NbActiveCorp:            len(activatedDevicesCorp),
			NbActiveBilly:           len(activatedDevicesBilly),
			NbRenewalCorp:           len(renewalDevicesCorp),
			NbRenewalBilly:          len(renewalDevicesBilly),
			NbCorpNotInBilly:        len(corpsNotInBilly),
			NbBillyNotInCorp:        len(billyNotInCorp),
			LstDeviceCorpNotInBilly: corpsNotInBilly,
			LstDeviceBillyNotInCorp: billyNotInCorp,
		}

		fileReport := filepath.Join(baseDir, name, "report.json")
		s, err := json.Marshal(report)
		utils.CheckErr(err)
		err = ioutil.WriteFile(fileReport, s, 0666)
		utils.CheckErr(err)

		// Dump report and contract to index.html
		index, err := ioutil.ReadFile("assets/index.html")
		utils.CheckErr(err)

		strContract, err := json.Marshal(contract.Data[0])
		utils.CheckErr(err)

		newContent := strings.Replace(string(index), "[CONTRACT]", string(strContract), -1)
		newContent = strings.Replace(newContent, "[REPORT]", string(s), -1)
		newContent = strings.Replace(newContent, "[TITLE]", name, -1)

		fileIndex := filepath.Join(baseDir, name, name+".html")
		err = ioutil.WriteFile(fileIndex, []byte(newContent), 0666)
		utils.CheckErr(err)

		log.Printf("Contract %s (%s) processed", name, contract.Data[0].ID)
	}

}
