package redshift

import (
	"database/sql"
	"fmt"
	"sofrance.fr/gnn/src/repository"
	"time"
)

const (
	dPB      = "2006-01-02 15:04:05.000000"
	host     = "so-fr-redshift-ct1.cfgljnlpxjsz.eu-west-1.redshift.amazonaws.com"
	port     = 5439
	user     = "sofrredshuser"
	password = "Riutdis57++"
	dbname   = "devices"
)

func Connect() (*sql.DB, error) {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	return sql.Open("postgres", psqlInfo)
}

func BuildDeviceRequest(contract *repository.Contract, year, month int) string {
	ret := fmt.Sprintf(
		//"SELECT device_id_hex,token_contractid,token_starttime,nvl(token_endtime,'%s'),startinmonth,startinmonthwithoutyear,month,year,nipidactivation,isbillable FROM billable_devices WHERE isbillable=1 AND token_contractid='%s' AND token_starttime BETWEEN '%s' AND '%s'",
		"SELECT device_id_hex,token_contractid,token_starttime,nvl(token_endtime,'%s'),startinmonth,startinmonthwithoutyear,month,year,nipidactivation,isbillable FROM billable_devices WHERE token_contractid='%s' AND month=%d AND year=%d",
		time.Time{}.Format(dPB), contract.Data[0].ID, month, year,
	)
	return ret
}
