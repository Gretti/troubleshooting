package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sofrance.fr/gnn/src/backend"
	"strconv"
)

const (
	replayDir = "C:\\Gretti\\work\\Billy\\Replay"
	username  = "5c86800625643267a9c42419"
	passwd    = "42eb21fa04c2297210292eeae42f397d"
)

func main() {
	year := 2021
	month := 12
	contracts := []string{
		"atelier_33a5_3eac",
	}
	monthDir := filepath.Join(replayDir, strconv.Itoa(year)+"_"+strconv.Itoa(month))
	if _, err := os.Stat(monthDir); os.IsNotExist(err) {
		err = os.Mkdir(monthDir, 0666)
	}

	for _, name := range contracts {
		contract, err := backend.FindContract(replayDir, name, username, passwd)
		if err != nil || len(contract.Data) == 0 {
			log.Printf("Contract %s not found! Skipping...", name)
			continue
		}

		jsonReplay := filepath.Join(replayDir, strconv.Itoa(year)+"_"+strconv.Itoa(month), name+".json")
		jsonString := "{\"contractId\": \"%s\", \"year\": \"%d\", \"month\": \"%d\"}"
		sprint := fmt.Sprintf(jsonString, contract.Data[0].ID, year, month)

		err = ioutil.WriteFile(jsonReplay, []byte(sprint), 0666)
		if err != nil {
			log.Fatal(err)
		}
	}
}
