package backend

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	repo "sofrance.fr/gnn/src/repository"
)

//nolint:funlen
//nolint:gocognit
func FindDevices(contractID, baseDir, name, username, passwd string) (*repo.DevicesCorp, error) {
	log.Printf("Retrieving the devices of contract %s", contractID)

	jsonDevices := filepath.Join(baseDir, name, "devices.json")
	var devices repo.DevicesCorp
	if _, err := os.Stat(jsonDevices); os.IsNotExist(err) {
		s, er := requestBackEnd(fmt.Sprintf("https://api.sigfox.com/v2/contract-infos/%s/devices", contractID), username, passwd)
		if er != nil {
			return nil, er
		}

		er = json.Unmarshal([]byte(s), &devices)
		if er != nil {
			return nil, er
		}

		np := 1

		if len(devices.Paging.Next) > 0 {
			var beURL, lastURL string
			var moreDevices repo.DevicesCorp
			for {
				if np == 1 {
					beURL = devices.Paging.Next
					lastURL = beURL
				} else {
					if lastURL == beURL {
						break
					}
					lastURL = beURL
					beURL = moreDevices.Paging.Next
				}

				fmt.Printf("Page %d\n", np)

				s, er = requestBackEnd(beURL, username, passwd)
				if er != nil {
					return nil, er
				}

				er = json.Unmarshal([]byte(s), &moreDevices)
				if er != nil {
					return nil, er
				}

				devices.Data = append(devices.Data, moreDevices.Data...)

				beURL = moreDevices.Paging.Next

				np++
			}
		}
		b, er := json.Marshal(devices)
		if er != nil {
			return nil, er
		}

		er = ioutil.WriteFile(jsonDevices, b, 0444)
		if er != nil {
			return nil, er
		}
	} else {
		b, er := ioutil.ReadFile(jsonDevices)
		if er != nil {
			return nil, err
		}
		_ = json.Unmarshal(b, &devices)
	}

	return &devices, nil
}

func FindContract(baseDir, name, username, passwd string) (*repo.Contract, error) {
	jsonContract := filepath.Join(baseDir, name, "contract.json")
	var contract repo.Contract
	if _, err := os.Stat(jsonContract); os.IsNotExist(err) {
		if _, err := os.Stat(filepath.Join(baseDir, name)); os.IsNotExist(err) {
			_ = os.MkdirAll(filepath.Join(baseDir, name), 0666)
		}
		log.Printf("Retrieving information about the contract %s", name)
		s, er := requestBackEnd(fmt.Sprintf("https://api.sigfox.com/v2/contract-infos/?name=%s", url.QueryEscape(name)), username, passwd)
		if er != nil {
			return nil, er
		}
		er = ioutil.WriteFile(jsonContract, []byte(s), 0444)
		if er != nil {
			return nil, er
		}
		_ = json.Unmarshal([]byte(s), &contract)
	} else {
		b, er := ioutil.ReadFile(jsonContract)
		if er != nil {
			return nil, er
		}
		er = json.Unmarshal(b, &contract)
		if er != nil {
			return nil, er
		}
	}
	return &contract, nil
}

func requestBackEnd(url, username, passwd string) (string, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}

	req.SetBasicAuth(username, passwd)

	resp, err := client.Do(req) //nolint:bodyclose
	if err != nil {
		return "", err
	}

	bodyText, _ := ioutil.ReadAll(resp.Body)
	s := string(bodyText)

	return s, nil
}
