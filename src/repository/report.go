package repository

import (
	"encoding/json"
	"log"
)

type Report struct {
	NbDevicesCorp           int           `json:"nb_devices_corp"`
	NbDevicesBilly          int           `json:"nb_devices_billy"`
	NbActiveCorp            int           `json:"nb_dev_active_corp"`
	NbActiveBilly           int           `json:"nb_dev_active_billy"`
	NbRenewalCorp           int           `json:"nb_dev_active_corp"`
	NbRenewalBilly          int           `json:"nb_dev_active_billy"`
	NbCorpNotInBilly        int           `json:"nb_corp_not_in_billy"`
	NbBillyNotInCorp        int           `json:"nb_billy_not_in_corp"`
	LstDeviceCorpNotInBilly []DeviceCorp  `json:"lst_dev_corp_not_in_billy"`
	LstDeviceBillyNotInCorp []DeviceBilly `json:"lst_dev_billy_not_in_corp"`
}

func (r Report) String() string {
	m, err := json.Marshal(r)
	if err != nil {
		log.Fatal(err)
	}
	return string(m)
}
