package repository

import (
	"encoding/json"
	"log"
	"time"
)

type DeviceCorp struct {
	ID               string `json:"id"`
	Name             string `json:"name"`
	SatelliteCapable bool   `json:"satelliteCapable"`
	Repeater         bool   `json:"repeater"`
	MessageModulo    int    `json:"messageModulo"`
	DeviceType       struct {
		ID        string   `json:"id"`
		Name      string   `json:"name,omitempty"`
		Actions   []string `json:"actions,omitempty"`
		Resources []string `json:"resources,omitempty"`
	} `json:"deviceType"`
	Contract struct {
		ID        string   `json:"id"`
		Name      string   `json:"name"`
		Actions   []string `json:"actions,omitempty"`
		Resources []string `json:"resources,omitempty"`
	} `json:"contract"`
	Group struct {
		ID      string   `json:"id"`
		Name    string   `json:"name,omitempty"`
		Type    int      `json:"type,omitempty"`
		Level   int      `json:"level,omitempty"`
		Actions []string `json:"actions,omitempty"`
	} `json:"group"`
	ModemCertificate struct {
		ID  string `json:"id"`
		Key string `json:"key,omitempty"`
	} `json:"modemCertificate"`
	Prototype          bool `json:"prototype"`
	ProductCertificate struct {
		ID  string `json:"id"`
		Key string `json:"key,omitempty"`
	} `json:"productCertificate"`
	Location struct {
		Lat float64 `json:"lat,omitempty"`
		Lng float64 `json:"lng,omitempty"`
	} `json:"location,omitempty"`
	LastComputedLocation struct {
		Lat        float64  `json:"lat,omitempty"`
		Lng        float64  `json:"lng,omitempty"`
		Radius     int      `json:"radius,omitempty"`
		Sourcecode int      `json:"sourceCode,omitempty"`
		PlaceIDs   []string `json:"placeIds,omitempty"`
	} `json:"lastComputedLocation,omitempty"`
	Pac                 string `json:"pac,omitempty"`
	SequenceNumber      int    `json:"sequenceNumber,omitempty"`
	TrashSequenceNumber int    `json:"trashSequenceNumber,omitempty"`
	LastCom             int    `json:"lastCom"`
	LQI                 int    `json:"lqi"`
	ActivationTime      int64  `json:"activationTime"`
	CreationTime        int64  `json:"creationTime"`
	State               int    `json:"state"`
	ComState            int    `json:"comState"`
	Token               struct {
		State              int    `json:"state"`
		DetailMessage      string `json:"detailMessage"`
		End                int64  `json:"end"`
		UnsubscriptionTime int64  `json:"unsubscriptionTime"`
		FreeMessages       int    `json:"freeMessages"`
		FreeMessagesSent   int    `json:"freeMessagesSent"`
	} `json:"token"`
	UnsubscriptionTime     int64    `json:"unsubscriptionTime"`
	CreatedBy              string   `json:"createdBy"`
	LastEditionTime        int      `json:"lastEditionTime"`
	LastEditedBy           string   `json:"lastEditedBy"`
	AutomaticRenewal       bool     `json:"automaticRenewal"`
	AutomaticRenewalStatus int      `json:"automaticRenewalStatus"`
	Activable              bool     `json:"activable"`
	Actions                []string `json:"actions,omitempty"`
	Resources              []string `json:"resources,omitempty"`
}

// DevicesCorp List of devices returned by Corp API Backend
type DevicesCorp struct {
	Data   []DeviceCorp `json:"data"`
	Paging struct {
		Next string `json:"next,omitempty"`
	} `json:"paging,omitempty"`
}

func (d DevicesCorp) String() string {
	marshal, err := json.MarshalIndent(d, "", "\t")
	if err != nil {
		log.Fatal(err)
	}
	return string(marshal)
}

func (d DeviceCorp) String() string {
	marshal, err := json.Marshal(d)
	if err != nil {
		log.Fatal(err)
	}
	return string(marshal)
}

//DeviceCorp "Activation date";"Com status";"Creation time";"Id";"Last seen";"Token end date ";"Token state"
type DeviceBilly struct {
	IdHex                   string
	ContractID              string
	TokenStartTime          time.Time
	TokenEndTime            time.Time
	StartInMonth            int
	StartInMonthWithoutYear int
	Month                   int
	Year                    int
	NipIDActivation         string
	IsBillable              int
}

func (d DeviceBilly) String() string {
	m, err := json.Marshal(d)
	if err != nil {
		log.Fatal(err)
	}
	return string(m)
}

func Diff(dCorp *[]DeviceCorp, dBilly *[]DeviceBilly) ([]DeviceCorp, []DeviceBilly) {
	dBillyNotInCorp := make([]DeviceBilly, 0)
	dCorpNotInBilly := make([]DeviceCorp, 0)

	//Device Billy not in Corp
	for _, db := range *dBilly {
		found := false
		for _, dc := range *dCorp {
			if db.IdHex == dc.ID {
				found = true
				break
			}
		}
		if !found {
			dBillyNotInCorp = append(dBillyNotInCorp, db)
		}
	}

	for _, dc := range *dCorp {
		found := false
		for _, db := range *dBilly {
			if db.IdHex == dc.ID {
				found = true
				break
			}
		}
		if !found {
			dCorpNotInBilly = append(dCorpNotInBilly, dc)
		}
	}

	return dCorpNotInBilly, dBillyNotInCorp
}
