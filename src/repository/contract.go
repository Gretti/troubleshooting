package repository

// Contract Contract info returned by Corp API Backend
type Contract struct {
	Data []struct {
		Name                 string `json:"name"`
		ActivationEndTime    int    `json:"activationEndTime"`
		CommunicationEndTime int    `json:"communicationEndTime"`
		BiDir                bool   `json:"bidir"`
		HighPriorityDownlink bool   `json:"highPriorityDownlink"`
		MaxUplinkFrames      int    `json:"maxUplinkFrames"`
		MaxDownlinkFrames    int    `json:"maxDownlinkFrames"`
		MaxTokens            int    `json:"maxTokens"`
		AutomaticRenewal     bool   `json:"automaticRenewal"`
		RenewalDuration      int    `json:"renewalDuration"`
		Options              []struct {
			ID         string `json:"id"`
			Parameters struct {
				Duration int `json:"duration,omitempty"`
				Nb       int `json:"nb,omitempty"`
			} `json:"parameters"`
		} `json:"options"`
		ID         string `json:"id"`
		ContractID string `json:"contractId"`
		UserID     string `json:"userId"`
		Group      struct {
			ID      string   `json:"id"`
			Name    string   `json:"name"`
			Type    int      `json:"type"`
			Level   int      `json:"level"`
			Actions []string `json:"actions"`
		} `json:"group"`
		Order struct {
			ID        string   `json:"id"`
			Name      string   `json:"name"`
			Actions   []string `json:"actions"`
			Resources []string `json:"resources"`
		} `json:"order"`
		PricingModel           int    `json:"pricingModel"`
		CreatedBy              string `json:"createdBy"`
		LastEditionTime        int    `json:"lastEditionTime"`
		CreationTime           int    `json:"creationTime"`
		LastEditedBy           string `json:"lastEditedBy"`
		StartTime              int    `json:"startTime"`
		Timezone               string `json:"timezone"`
		SubscriptionPlan       int    `json:"subscriptionPlan"`
		TokenDuration          int    `json:"tokenDuration"`
		BlacklistedTerritories []struct {
			ID      string   `json:"id"`
			Name    string   `json:"name"`
			Type    int      `json:"type"`
			Level   int      `json:"level"`
			Actions []string `json:"actions"`
		} `json:"blacklistedTerritories"`
		TokensInUse int `json:"tokensInUse"`
		TokensUsed  int `json:"tokensUsed"`
	}
}
